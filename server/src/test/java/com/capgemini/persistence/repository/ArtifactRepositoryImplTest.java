package com.capgemini.persistence.repository;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.common.datatypes.enumerations.Status;
import com.capgemini.common.datatypes.to.SearchCriteriaTo;
import com.capgemini.persistence.entity.Artifact;
import com.capgemini.persistence.repository.impl.ArtifactRepositoryImpl;

import org.junit.Assert;

public class ArtifactRepositoryImplTest {
	private ArtifactRepository repository;
	private Long id = 1L;

	@Before
	public void setup() {
		repository = new ArtifactRepositoryImpl();
	}

	@Test 
	public void bookArtifactTest(){
		//given 
		Artifact artifact = new Artifact();
		//when
		artifact= repository.findArtifactById(id);
		artifact.setStatus(Status.AVAILABLE);
		repository.bookArtifact(id);
		//then
		Assert.assertEquals(artifact.getStatus(), Status.BOOKED);
		
		
	}
	@Test 
	public void borrowArtifactTest(){
		//given 
		Artifact artifact = new Artifact();
		//when
		artifact= repository.findArtifactById(id);
		artifact.setStatus(Status.AVAILABLE);
		repository.borrowArtifact(id);
		//then
		Assert.assertEquals(artifact.getStatus(), Status.BORROWED);
		
		
	}
	@Test 
	public void returnArtifactTest(){
		//given 
		Artifact artifact = new Artifact();
		//when
		artifact= repository.findArtifactById(id);
		repository.returnArtifact(id);
		//then
		Assert.assertEquals(artifact.getStatus(), Status.AVAILABLE);
		
	}
}