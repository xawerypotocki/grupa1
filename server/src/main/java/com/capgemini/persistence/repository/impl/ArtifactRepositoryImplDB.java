package com.capgemini.persistence.repository.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capgemini.common.datatypes.enumerations.Genre;
import com.capgemini.common.datatypes.enumerations.Status;
import com.capgemini.common.datatypes.to.SearchCriteriaTo;
import com.capgemini.persistence.entity.Artifact;
import com.capgemini.persistence.repository.ArtifactRepository;

/**
 * Repository used for Database actions with the Artifact Entity.
 * 
 * @author CWOJTOWI
 */
// @Repository
// @Primary
public interface ArtifactRepositoryImplDB extends JpaRepository<Artifact, Long>, ArtifactRepository {

	public static final String EMPTY_STRING = "";

	public Artifact findArtifactById(Long id);

	default public List<Artifact> findArtifacts(SearchCriteriaTo searchCriteriaTo) {
		if (searchCriteriaTo == null) {
			return findAll();
		}
		sanitizeSearchCriteriaTo(searchCriteriaTo);
		return findAll().stream()
				.filter(a -> searchCriteriaTo.getAuthor().equals(EMPTY_STRING) ? true
						: a.getAuthor().equalsIgnoreCase(searchCriteriaTo.getAuthor()))
				.filter(a -> searchCriteriaTo.getTitle().equals(EMPTY_STRING) ? true
						: a.getTitle().equalsIgnoreCase(searchCriteriaTo.getTitle()))
				.filter(a -> searchCriteriaTo.getGenre() == null ? true
						: a.getGenre().equals(searchCriteriaTo.getGenre()))
				.collect(Collectors.toList());

	}

	public default SearchCriteriaTo sanitizeSearchCriteriaTo(SearchCriteriaTo searchCriteriaTo) {
		if (searchCriteriaTo.getAuthor() == null)
			searchCriteriaTo.setAuthor(EMPTY_STRING);
		if (searchCriteriaTo.getTitle() == null)
			searchCriteriaTo.setTitle(EMPTY_STRING);
		return searchCriteriaTo;
	}

	public List<Artifact> findAll();

	public List<Artifact> findArtifactsByGenre(Genre genre);

	public Artifact getOne(Long id);

	default public Artifact createArtifact(Artifact artifact) {
		return save(artifact);
	}

	default public boolean bookArtifact(Long id) {
		Artifact artifact = getOne(id);
		boolean success = false;
		if (artifact.getStatus() == Status.AVAILABLE) {
			artifact.setStatus(Status.BOOKED);
			save(artifact);
			success = true;
		}
		return success;
	}

	default public boolean borrowArtifact(Long id) {
		Artifact artifact = getOne(id);
		boolean success = false;
		if (artifact.getStatus() != Status.BORROWED) {
			artifact.setStatus(Status.BORROWED);
			save(artifact);
			success = true;
		}
		return success;
	}

	default public boolean returnArtifact(Long id) {
		Artifact artifact = getOne(id);
		boolean success = false;
		if (artifact.getStatus() != Status.AVAILABLE) {
			artifact.setStatus(Status.AVAILABLE);
			success = true;
		}
		return success;
	}

	public void delete(Long id);

	public void delete(Artifact artifact);

	public void deleteAll();
}
