package com.capgemini.persistence.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capgemini.common.datatypes.enumerations.UserType;
import com.capgemini.persistence.entity.User;
import com.capgemini.persistence.repository.UserRepository;

//@Repository
//@Primary
public interface UserRepositoryImplDB extends JpaRepository<User, Long>, UserRepository {
	static final Map<String, UUID> userSessions = new HashMap<String, UUID>();

	public default boolean isUserAuthorized(UUID sessionID) {
		return userSessions.containsValue(sessionID);
	}

	public List<User> findUserByUsername(String username);

	public default UUID login(String username, String password) {
		List<User> users = findUserByUsername(username);

		Optional<User> foundUser = users.stream().filter(user -> user.getUsername().equals(username)).findAny();

		if (foundUser.isPresent() && foundUser.get().getPassword().equals(password)) {
			if (userSessions.containsKey(username)) {
				return userSessions.get(username);
			} else {
				UUID generatedID = UUID.randomUUID();
				userSessions.put(username, generatedID);
				return generatedID;
			}
		}
		return null;
	}

	public default boolean register(User newUser) {
		if (userAlreadyExists(newUser)) {
			return false;
		}
		save(newUser);
		return true;
	}

	public default void logout(UUID sessionID) {
		if (!isUserAuthorized(sessionID))
			return;
	}

	public default boolean userAlreadyExists(User newUser) {
		List<User> users = findUserByUsername(newUser.getUsername());
		return !users.isEmpty();
	}

	public default UserType getUserType(String username) {
		List<User> users = findUserByUsername(username);
		Optional<User> foundUser = users.stream().filter(user -> user.getUsername().equals(username)).findAny();
		if (foundUser.isPresent()) {
			return foundUser.get().getUserType();
		}
		return null;
	}
}
