package com.capgemini.persistence.repository;

import java.util.List;

import com.capgemini.common.datatypes.to.SearchCriteriaTo;
import com.capgemini.persistence.entity.Artifact;

/**
 * Repository used for Database actions with the Artifact Entity.
 * 
 * @author CWOJTOWI
 */
public interface ArtifactRepository {

	public Artifact findArtifactById(Long id);

	public List<Artifact> findArtifacts(SearchCriteriaTo searchCriteriaTo);

	public Artifact createArtifact(Artifact artifact);

	public boolean deleteArtifactById(Long id);

	public boolean bookArtifact(Long id);

	public boolean borrowArtifact(Long artifactId);

	public boolean returnArtifact(Long artifactId);
}
