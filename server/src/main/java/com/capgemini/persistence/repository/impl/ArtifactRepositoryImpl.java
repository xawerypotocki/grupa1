package com.capgemini.persistence.repository.impl;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.capgemini.common.datatypes.enumerations.Genre;
import com.capgemini.common.datatypes.enumerations.Status;
import com.capgemini.common.datatypes.to.SearchCriteriaTo;
import com.capgemini.persistence.entity.Artifact;
import com.capgemini.persistence.repository.ArtifactRepository;

@Repository
@Primary
public class ArtifactRepositoryImpl implements ArtifactRepository {

	private static final Map<Long, Artifact> mockedData;
	private static final String EMPTY_STRING = "";

	static {
		mockedData = new HashMap<Long, Artifact>();
		mockedData.put(1L, new Artifact(1L, "Good Artifact", "Kornel Piatkowski"));
		mockedData.put(2L, new Artifact(2L, "Perfect Artifact", "Kacper Kasztelanic"));
		mockedData.put(3L, new Artifact(3L, "Sufficient Artifact", "Xavery Potocki"));
		mockedData.put(4L, new Artifact(4L, "New Good Artifact", "Kornel Piatkowski"));
		for (Entry<Long, Artifact> e : mockedData.entrySet()) {
			e.getValue().setGenre(Genre.IT);
			e.getValue().setStatus(Status.AVAILABLE);
		}
	}

	@Override
	public Artifact findArtifactById(Long id) {
		return mockedData.get(id);
	}

	@Override
	public List<Artifact> findArtifacts(SearchCriteriaTo searchCriteriaTo) {
		if (searchCriteriaTo == null) {
			return mockedData.values().stream().collect(Collectors.toList());
		}
		sanitizeSearchCriteriaTo(searchCriteriaTo);
		return mockedData.values().stream()
				.filter(a -> searchCriteriaTo.getAuthor().equals(EMPTY_STRING) ? true
						: a.getAuthor().equalsIgnoreCase(searchCriteriaTo.getAuthor()))
				.filter(a -> searchCriteriaTo.getTitle().equals(EMPTY_STRING) ? true
						: a.getTitle().equalsIgnoreCase(searchCriteriaTo.getTitle()))
				.filter(a -> searchCriteriaTo.getGenre() == null ? true
						: a.getGenre().equals(searchCriteriaTo.getGenre()))
				.collect(Collectors.toList());
	}

	private void sanitizeSearchCriteriaTo(SearchCriteriaTo searchCriteriaTo) {
		if (searchCriteriaTo.getAuthor() == null)
			searchCriteriaTo.setAuthor(EMPTY_STRING);
		if (searchCriteriaTo.getTitle() == null)
			searchCriteriaTo.setTitle(EMPTY_STRING);
	}

	@Override
	public Artifact createArtifact(Artifact artifact) {
		Long lastIndex = getNewIndex();
		artifact.setId(++lastIndex);
		mockedData.put(artifact.getId(), artifact);

		return artifact;
	}

	private Long getNewIndex() {
		return mockedData.keySet().stream().sorted(new Comparator<Long>() {
			@Override
			public int compare(Long o1, Long o2) {
				return o2.compareTo(o1);
			}
		}).findFirst().get();
	}

	@Override
	public boolean deleteArtifactById(Long id) {
		Iterator<Entry<Long, Artifact>> iterator = mockedData.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Long, Artifact> next = iterator.next();
			if (next.getValue().getId() == id) {
				iterator.remove();
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean bookArtifact(Long id) {
		Artifact artifact;
		artifact = mockedData.get(id);

		if (artifact == null) {
			return false;
		}

		if (artifact.getStatus() != Status.AVAILABLE) {
			return false;
		}

		mockedData.get(id).setStatus(Status.BOOKED);
		return true;
	}

	@Override
	public boolean borrowArtifact(Long artifactId) {
		Artifact artifact;
		artifact = mockedData.get(artifactId);

		if (artifact == null) {
			return false;
		}

		if (artifact.getStatus() == Status.BORROWED) {
			return false;
		}

		mockedData.get(artifactId).setStatus(Status.BORROWED);
		return true;
	}

	@Override
	public boolean returnArtifact(Long artifactId) {
		Artifact artifact;
		artifact = mockedData.get(artifactId);

		if (artifact == null) {
			return false;
		}

		if (artifact.getStatus() != Status.BORROWED) {
			return false;
		}

		mockedData.get(artifactId).setStatus(Status.AVAILABLE);
		return true;
	}
}
