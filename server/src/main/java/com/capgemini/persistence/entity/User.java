package com.capgemini.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.capgemini.common.datatypes.enumerations.UserType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

/**
 * User Entity
 * 
 * @author CWOJTOWI
 */
@Entity
public class User {

	@Id
	@GeneratedValue
	private Long id;

	@Size(max = 32)
	private String username;

	@Enumerated(EnumType.STRING)
	private UserType userType = UserType.USER;

	@Size(max = 32)
	private String password;

	@Size(max = 32)
	private String email;

	@Size(max = 32)
	private String firstname;

	@Size(max = 32)
	private String lastname;

	@Size(max = 256)
	private String lifeMotto;

	@Size(max = 256)
	private String aboutMe;

	public User() {

	}

	public User(Long id, UserType userType, String username, String password, String email) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
		this.userType = userType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLifeMotto() {
		return lifeMotto;
	}

	public void setLifeMotto(String lifeMotto) {
		this.lifeMotto = lifeMotto;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		if (userType != null)
			this.userType = userType;
	}
}
