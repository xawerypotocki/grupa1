package com.capgemini.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import com.capgemini.common.datatypes.enumerations.Genre;
import com.capgemini.common.datatypes.enumerations.Status;

/**
 * Artifact Entity
 * 
 * @author CWOJTOWI
 */
@Entity
public class Artifact {

	@Id
	@GeneratedValue
	private Long id;

	@Size(max = 32)
	private String author;

	@Size(max = 32)
	private String title;

	@Enumerated(EnumType.STRING)
	private Genre genre;

	@Enumerated(EnumType.STRING)
	private Status status;

	public Artifact() {

	}

	public Artifact(Long id, String title, String author) {
		this(id, title, author, Genre.IT, Status.AVAILABLE);
	}

	public Artifact(Long id, String title, String author, Genre genre, Status status) {
		this.id = id;
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.status = status;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Artifact [id=" + id + ", author=" + author + ", title=" + title + ", genre=" + genre + ", status="
				+ status + "]";
	}
}
