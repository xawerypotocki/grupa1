package com.capgemini.exceptions;

public class AlreadyReturnedException extends Exception {
	public AlreadyReturnedException() {
	}

	public AlreadyReturnedException(String message) {
		super(message);
	}
}
