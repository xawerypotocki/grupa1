package com.capgemini.exceptions;

public class AlreadyBookedException extends Exception {
	public AlreadyBookedException() {
	}

	public AlreadyBookedException(String message) {
		super(message);
	}
}
