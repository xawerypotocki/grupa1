package com.capgemini.exceptions;

public class AlreadyBorrowedException extends Exception {
	public AlreadyBorrowedException() {
	}

	public AlreadyBorrowedException(String message) {
		super(message);
	}
}
