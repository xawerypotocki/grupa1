package com.capgemini;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

import com.capgemini.common.datatypes.enumerations.UserType;
import com.capgemini.persistence.entity.User;
import com.capgemini.persistence.repository.ArtifactRepository;
import com.capgemini.persistence.repository.UserRepository;
import com.capgemini.utils.RandomArtifactGenerator;

@SpringBootApplication
// Comment out @EnableAutoConfiguration annotation and annotate
// ArtifactRepositoryImplDB as @Primary in order to use Database instead of a
// mock.
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class,
		DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
public class CrashcourseApplication {
	@Autowired
	ArtifactRepository artifactRepository;
	@Autowired
	UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(CrashcourseApplication.class, args);
	}

	@PostConstruct
	public void setup() {
		userRepository.register(new User(1L, UserType.ADMIN, "admin", "topsecret", "admin@capgemini.com"));
		userRepository.register(new User(2L, UserType.USER, "user", "user", "user@capgemini.com"));
		RandomArtifactGenerator gen = new RandomArtifactGenerator();
		for (int i = 0; i < 1000; i++)
			artifactRepository.createArtifact(gen.getRandomArtifact());
	}
}
