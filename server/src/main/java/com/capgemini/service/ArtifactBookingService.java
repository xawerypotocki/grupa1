package com.capgemini.service;

import javax.naming.AuthenticationException;

import com.capgemini.exceptions.AlreadyBookedException;

public interface ArtifactBookingService {
	Boolean bookArtifact(Long artifactId, String sessionId) throws AuthenticationException, AlreadyBookedException;
}
