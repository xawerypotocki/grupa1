package com.capgemini.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.naming.AuthenticationException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.common.datatypes.bo.UserBo;
import com.capgemini.persistence.entity.User;
import com.capgemini.persistence.repository.UserRepository;
import com.capgemini.service.AuthorizationService;
import com.capgemini.service.UserSearchService;

@Component
public class UserSearchServiceImpl implements UserSearchService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AuthorizationService authorizationService;

	private ModelMapper modelMapper = new ModelMapper();

	
	@Override
	public List<UserBo> findUsers(String sessionId) throws AuthenticationException {

		if (!authorizationService.isUserAuthorized(UUID.fromString(sessionId))) {
			throw new AuthenticationException();
		}
		
		List<User> foundEntities = userRepository.findUsers();
		List<UserBo> users = new ArrayList<UserBo>();
		
		for(User entity : foundEntities){
			users.add(modelMapper.map(entity, UserBo.class));
		}
		return users;
	}

}
