package com.capgemini.service.impl;

import java.util.UUID;

import javax.naming.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.exceptions.AlreadyBookedException;
import com.capgemini.exceptions.AlreadyBorrowedException;
import com.capgemini.exceptions.AlreadyReturnedException;
import com.capgemini.persistence.repository.ArtifactRepository;
import com.capgemini.service.ArtifactRentService;
import com.capgemini.service.AuthorizationService;

@Service
public class ArtifactRentServiceImpl implements ArtifactRentService {

	@Autowired
	private ArtifactRepository artifactRepository;

	@Autowired
	private AuthorizationService authorizationService;
	
	@Override
	public Boolean borrowArtifact(Long artifactId, Long userId, String sessionId)
			throws AuthenticationException, AlreadyBorrowedException {
		if (!authorizationService.isUserAuthorized(UUID.fromString(sessionId))) {
			throw new AuthenticationException();
		}

		if (!artifactRepository.borrowArtifact(artifactId)) {
			throw new AlreadyBorrowedException();
		} else
			return true;
	}

	@Override
	public Boolean returnArtifact(Long artifactId, String sessionId)
			throws AuthenticationException, AlreadyReturnedException {
		if (!authorizationService.isUserAuthorized(UUID.fromString(sessionId))) {
			throw new AuthenticationException();
		}

		if (!artifactRepository.returnArtifact(artifactId)) {
			throw new AlreadyReturnedException();
		} else
			return true;
	}

}
