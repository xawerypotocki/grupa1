package com.capgemini.service.impl;

import java.util.UUID;

import javax.naming.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.exceptions.AlreadyBookedException;
import com.capgemini.persistence.repository.ArtifactRepository;
import com.capgemini.service.ArtifactBookingService;
import com.capgemini.service.AuthorizationService;

@Service
public class ArtifactBookingServiceImpl implements ArtifactBookingService {

	@Autowired
	private ArtifactRepository artifactRepository;

	@Autowired
	private AuthorizationService authorizationService;

	@Override
	public Boolean bookArtifact(Long artifactId, String sessionId)
			throws AuthenticationException, AlreadyBookedException {
		if (!authorizationService.isUserAuthorized(UUID.fromString(sessionId))) {
			throw new AuthenticationException();
		}

		if (!artifactRepository.bookArtifact(artifactId)) {
			throw new AlreadyBookedException();
		} else
			return true;
	}
}
