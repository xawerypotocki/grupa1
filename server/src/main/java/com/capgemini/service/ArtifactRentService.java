package com.capgemini.service;

import javax.naming.AuthenticationException;

import com.capgemini.exceptions.AlreadyBorrowedException;
import com.capgemini.exceptions.AlreadyReturnedException;

public interface ArtifactRentService {

	Boolean borrowArtifact(Long artifactId, Long userId, String sessionId) throws AuthenticationException, AlreadyBorrowedException;
	Boolean returnArtifact(Long artifactId, String sessionId) throws AuthenticationException, AlreadyReturnedException;
}
