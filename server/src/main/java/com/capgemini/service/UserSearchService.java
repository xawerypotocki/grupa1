package com.capgemini.service;

import java.util.List;

import javax.naming.AuthenticationException;

import com.capgemini.common.datatypes.bo.UserBo;

public interface UserSearchService {
	List<UserBo> findUsers(String sessionId) throws AuthenticationException;
}
