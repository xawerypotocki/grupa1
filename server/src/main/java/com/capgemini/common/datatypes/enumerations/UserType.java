package com.capgemini.common.datatypes.enumerations;

public enum UserType {
	ADMIN, USER
}
