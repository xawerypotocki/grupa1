package com.capgemini.common.datatypes.to;

import java.util.UUID;

import com.capgemini.common.datatypes.enumerations.UserType;


/**
 * Session Transport Object
 * @author CWOJTOWI
 */
public class SessionTo {
    private UUID sessionId;
    
    private UserType userType;

    public SessionTo() {
    }

    public SessionTo(UUID sessionId) {
        this.sessionId = sessionId;
    }
    
    public SessionTo(UUID sessionId, UserType userType) {
    	this.sessionId = sessionId;
        this.userType = userType;
    }

    public UUID getSessionId() {
        return sessionId;
    }

    public void setSessionId(UUID sessionId) {
        this.sessionId = sessionId;
    }
    
    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userTyp) {
        this.userType = userTyp;
    }
}
