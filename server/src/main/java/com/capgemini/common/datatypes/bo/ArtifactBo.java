package com.capgemini.common.datatypes.bo;

import java.util.Date;

import com.capgemini.common.datatypes.enumerations.Genre;
import com.capgemini.common.datatypes.enumerations.Status;

/**
 * Artifact Business Object
 * 
 * @author CWOJTOWI
 */
public class ArtifactBo {

	private Long id;

	private String author;

	private String title;

	private Genre genre;

	private Status status;

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public Status getStatus() {
		return status;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
