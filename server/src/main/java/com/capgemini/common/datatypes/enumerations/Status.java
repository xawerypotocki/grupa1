package com.capgemini.common.datatypes.enumerations;

public enum Status {
	AVAILABLE,
    BOOKED,
    BORROWED
}
