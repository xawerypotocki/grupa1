package com.capgemini.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.capgemini.common.datatypes.enumerations.Genre;
import com.capgemini.common.datatypes.enumerations.Status;
import com.capgemini.persistence.entity.Artifact;

public final class RandomArtifactGenerator {
	private static final String NAMES_FILE = "names.txt";
	private static final String BOOKS_FILE = "book_titles.txt";

	private final Random random = new Random();
	private final List<String> names;
	private final List<String> titles;

	public RandomArtifactGenerator() {
		names = readLines(getFile(NAMES_FILE));
		titles = readLines(getFile(BOOKS_FILE));
	}

	public Artifact getRandomArtifact() {
		Artifact artifact = new Artifact();
		artifact.setAuthor(getRandomName());
		artifact.setTitle(getRandomTitle());
		artifact.setGenre(getRandomGenre());
		artifact.setStatus(Status.AVAILABLE);
		return artifact;
	}

	private String getRandomName() {
		return names.get(random.nextInt(names.size()));
	}

	private String getRandomTitle() {
		return titles.get(random.nextInt(titles.size()));
	}

	private Genre getRandomGenre() {
		int genreLength = Genre.values().length;
		return Genre.values()[random.nextInt(genreLength)];
	}

	private List<String> readLines(File file) {
		List<String> lines = new ArrayList<String>();
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
		}
		return lines;
	}

	private File getFile(String fileName) {
		ClassLoader classLoader = getClass().getClassLoader();
		return new File(classLoader.getResource(fileName).getFile());
	}
}
