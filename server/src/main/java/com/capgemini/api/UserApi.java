package com.capgemini.api;

import java.util.List;
import org.springframework.http.ResponseEntity;
import com.capgemini.common.datatypes.to.UserTo;

public interface UserApi {


	ResponseEntity<List<UserTo>> getAllUsers(String sessionId);
	
	
}
