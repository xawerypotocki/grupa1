package com.capgemini.api;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.capgemini.common.datatypes.to.ArtifactTo;
import com.capgemini.common.datatypes.to.SearchCriteriaTo;

/**
 * REST API for the Artifact objects.
 * 
 * @author CWOJTOWI
 */
public interface ArtifactApi {

	ResponseEntity<List<ArtifactTo>> getArtifacts(SearchCriteriaTo searchCriteriaTo, String sessionId);

	ResponseEntity<ArtifactTo> addNewArtifact(ArtifactTo incomingArtifactTo, String sessionId);

	ResponseEntity<Boolean> bookArtifact(Long artifactId, String sessionId);
	
	ResponseEntity<Boolean> borrowArtifact(Long artifactId, Long userId, String sessionId);
	
	ResponseEntity<Boolean> returnArtifact(Long artifactId, String sessionId);
}
