package com.capgemini.api.impl;

import java.util.ArrayList;
import java.util.List;

import javax.naming.AuthenticationException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.api.UserApi;
import com.capgemini.common.datatypes.bo.UserBo;
import com.capgemini.common.datatypes.to.UserTo;
import com.capgemini.service.UserSearchService;


@CrossOrigin
@RestController
@RequestMapping("/rest/artifactlibrary/component/v1")
public class UserApiImpl implements UserApi {

	@Autowired
	private UserSearchService userSearchService;

	private ModelMapper modelMapper = new ModelMapper();

	@Override
	@RequestMapping(value = "/users/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserTo>> getAllUsers(@RequestHeader(value = "SessionID") String sessionId) {
		List<UserBo> users = null;
		try {
			users = userSearchService.findUsers(sessionId);
		} catch (AuthenticationException e) {
			return new ResponseEntity<List<UserTo>>(HttpStatus.UNAUTHORIZED);
		}

		List<UserTo> results = new ArrayList<>();

		for (UserBo user : users) {
			results.add(modelMapper.map(user, UserTo.class));
		}

		return new ResponseEntity<List<UserTo>>(results, HttpStatus.OK);
	}

}
