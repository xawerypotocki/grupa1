package com.crashcourse.restclient.api.impl;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.naming.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.crashcourse.restclient.api.ArtifactRestServiceClient;
import com.crashcourse.restclient.datatype.ArtifactTo;
import com.crashcourse.restclient.datatype.SearchCriteriaTo;
import com.crashcourse.restclient.datatype.SessionTo;
import com.crashcourse.restclient.exceptions.AlreadyBookedException;
import com.crashcourse.restclient.main.config.LibrarySecurityContext;

@Component
public class ArtifactRestServiceClientImpl implements ArtifactRestServiceClient {

	@Value("${application.service.url}")
	private String serviceUrl;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private LibrarySecurityContext app;

	@Override
	public List<ArtifactTo> getArtifacts(SearchCriteriaTo searchCriteriaTo){

		if (searchCriteriaTo == null) {
			searchCriteriaTo = new SearchCriteriaTo();
		}
		RequestEntity<SearchCriteriaTo> requestEntity = new RequestEntity<>(searchCriteriaTo, buildRequestHeader(),
				HttpMethod.POST, builAllArtifactsRequestUri());

		ResponseEntity<List<ArtifactTo>> exchange = restTemplate.exchange(requestEntity,
				new ParameterizedTypeReference<List<ArtifactTo>>() {
				});
		return exchange.getBody();
	}

	private <T extends Object> RequestEntity<T> builArtifactsRequest(URI uri, T body, HttpMethod method) {
		HttpHeaders head = buildRequestHeader();

		return new RequestEntity<>(body, head, method, uri);
	}

	private URI builAllArtifactsRequestUri() {
		return URI.create(new StringBuilder().append(serviceUrl).append("/artifacts/").toString());
	}

	private URI builBookArtifactRequestUri() {
		return URI.create(new StringBuilder().append(serviceUrl).append("/book-artifact/").toString());
	}

	private URI builSearchArtifactRequestUri() {
		return URI.create(new StringBuilder().append(serviceUrl).append("/search-artifact/").toString());
	}

	private URI builAddArtifactRequestUri() {
		return URI.create(new StringBuilder().append(serviceUrl).append("/id/").toString());
	}

	private HttpHeaders buildRequestHeader() {
		HttpHeaders head = new HttpHeaders();
		head.add("SessionID",
				Optional.ofNullable(app.getSession()).map(SessionTo::getSessionId).map(UUID::toString).orElse(null));
		return head;
	}

	@Override
	public void addArtifact(ArtifactTo artifact) {
		RequestEntity<ArtifactTo> request = builArtifactsRequest(builAddArtifactRequestUri(), artifact,
				HttpMethod.POST);

		restTemplate.exchange(request, new ParameterizedTypeReference<ArtifactTo>() {
		});
	}

	@Override
	public void reserve(Long id) throws AuthenticationException, AlreadyBookedException{
		RequestEntity<Long> requestEntity = this.<Long> builArtifactsRequest(builBookArtifactRequestUri(), id,
				HttpMethod.POST);

		ResponseEntity<Boolean> exchange = restTemplate.exchange(requestEntity, new ParameterizedTypeReference<Boolean>() {
		});
		
		if(exchange.getBody().equals(Boolean.FALSE) && exchange.getStatusCode().equals(HttpStatus.OK)){
			throw new AlreadyBookedException();
		}else if(exchange.getBody().equals(Boolean.FALSE) && exchange.getStatusCode().equals(HttpStatus.UNAUTHORIZED)){
			throw new AuthenticationException();
		}
	}

	@Override
	public List<ArtifactTo> searchBySearchCriteria(SearchCriteriaTo searchCriteria) {
		RequestEntity<SearchCriteriaTo> requestEntity = builArtifactsRequest(builSearchArtifactRequestUri(),
				searchCriteria, HttpMethod.POST);

		ResponseEntity<List<ArtifactTo>> exchange = restTemplate.exchange(requestEntity,
				new ParameterizedTypeReference<List<ArtifactTo>>() {
				});
		
		

		return exchange.getBody();
	}

	@Override
	public void rent(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void free(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void book(Long id) {
		// TODO Auto-generated method stub
		
	}

}
