package com.crashcourse.restclient.api;

import java.util.List;

import javax.naming.AuthenticationException;

import com.crashcourse.restclient.datatype.ArtifactTo;
import com.crashcourse.restclient.datatype.SearchCriteriaTo;
import com.crashcourse.restclient.exceptions.AlreadyBookedException;
import com.crashcourse.restclient.exceptions.NoItemSelected;

/**
 * REST client for the artifact REST service.
 *
 * @author Mateusz
 */
public interface ArtifactRestServiceClient {

    /**
     * Gets all artifacts..
     *
     * @return list of artifacts
     */
    List<ArtifactTo> getArtifacts(SearchCriteriaTo searchCriteriaTo);

    /**
     * Saves a new artifact.
     *
     * @param artifact the artifact to save
     * @return saved artifact
     */
    void addArtifact(ArtifactTo artifact);
    
    /**
     * Reserves a new artifact.
     *
     * @param id the artifact to reserve
     * @return reserved artifact
     */
    void reserve (Long id) throws AuthenticationException, AlreadyBookedException;
    
    /**
     * Get artifacts, which fulfill Search Criteria
     *
     * @param searchCriteria search criteria
     * @return list of artifacts 
     */
    List<ArtifactTo> searchBySearchCriteria(SearchCriteriaTo searchCriteria);

	void rent(Long id);

	void free(Long id);

	void book(Long id);
    

}
