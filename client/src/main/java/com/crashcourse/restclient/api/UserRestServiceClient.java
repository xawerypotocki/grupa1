package com.crashcourse.restclient.api;

import java.util.List;

import com.crashcourse.restclient.datatype.UserTo;

public interface UserRestServiceClient {

	List<UserTo> getAllUsers();
	
	void addUser(UserTo userTo);
	
}
