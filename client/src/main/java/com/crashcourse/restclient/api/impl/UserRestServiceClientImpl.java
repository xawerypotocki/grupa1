package com.crashcourse.restclient.api.impl;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.crashcourse.restclient.api.UserRestServiceClient;
import com.crashcourse.restclient.datatype.SessionTo;
import com.crashcourse.restclient.datatype.UserTo;
import com.crashcourse.restclient.main.config.LibrarySecurityContext;

@Component
public class UserRestServiceClientImpl implements UserRestServiceClient{
	
	@Value("${application.service.url}")
	private String serviceUrl;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private LibrarySecurityContext app;

	@Override
	public List<UserTo> getAllUsers() {

		RequestEntity<Void> requestEntity = new RequestEntity<>(null, buildRequestHeader(),
				HttpMethod.POST, buildAllUsersRequestUri());
		//TODO jak serwer zaimplementuje
//		ResponseEntity<List<UserTo>> exchange = restTemplate.exchange(requestEntity,
//				new ParameterizedTypeReference<List<UserTo>>() {
//				});
		return null;			
	}

	private URI buildAllUsersRequestUri() {
		return URI.create(new StringBuilder().append(serviceUrl).append("/users/").toString());
	}

	private HttpHeaders buildRequestHeader() {
		HttpHeaders head = new HttpHeaders();
		head.add("SessionID",
				Optional.ofNullable(app.getSession()).map(SessionTo::getSessionId).map(UUID::toString).orElse(null));
		return head;
	}

	@Override
	public void addUser(UserTo userTo) {
		// TODO Auto-generated method stub
		
	}

}
