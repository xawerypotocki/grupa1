package com.crashcourse.restclient.controller;

import javafx.scene.control.Button;
import java.util.List;
import java.util.stream.Collectors;

import javax.naming.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;

import com.crashcourse.restclient.api.ArtifactRestServiceClient;
import com.crashcourse.restclient.api.UserRestServiceClient;
import com.crashcourse.restclient.datatype.ArtifactTo;
import com.crashcourse.restclient.datatype.SearchCriteriaTo;
import com.crashcourse.restclient.datatype.UserTo;
import com.crashcourse.restclient.datatype.enumeration.Genre;
import com.crashcourse.restclient.datatype.enumeration.Status;
import com.crashcourse.restclient.model.ArtifactModel;
import com.crashcourse.restclient.model.UserModel;
import com.crashcourse.restclient.view.FXMLDialog;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ArtifactListAdminController extends ArtifactsBaseController {

    @FXML
    private TableView<ArtifactModel> artifacts;
    @FXML
    private TableView<UserModel> users;
    @FXML 
    TableColumn<ArtifactModel, String> idColumn;
    @FXML
    TableColumn<ArtifactModel, String> titleColumn;
    @FXML
    TableColumn<ArtifactModel, String> authorColumn;
    @FXML
    TableColumn<ArtifactModel, Genre> genreColumn;
    @FXML
    TableColumn<ArtifactModel, Status> statusColumn;
    @FXML
    TextField authorInput;
    @FXML
    ComboBox<Genre> genreInput;
    @Autowired
    private ArtifactRestServiceClient restServiceClient;
    
    @Autowired
    private UserRestServiceClient userRestServiceClient;
    @FXML
    TextField titleInput;

	@FXML Button bookBtn;
	@FXML Button rentBtn;
	@FXML Button freeBtn;
	
	@FXML TableColumn<UserModel, String> userIdColumn;
	@FXML TableColumn<UserModel, String> userFirstName;
	@FXML TableColumn<UserModel, String> userLastName;
	
	
    public ArtifactListAdminController(Stage primaryStage) {
        super(primaryStage);
    }

    @FXML
    void initialize() {
    	//sprzezenie danych z modeluem
    	
    	//pusczenie metdy na oddzielny watku
    	Platform.runLater(()->{
    		
    		loadAllData();

    		titleColumn.setCellValueFactory(celldata -> celldata.getValue().getTitle());
    		authorColumn.setCellValueFactory(celldata -> celldata.getValue().getAuthor());
    		genreColumn.setCellValueFactory(celldata -> celldata.getValue().getGenre());
    		statusColumn.setCellValueFactory(celldata -> celldata.getValue().getStatus());
    		genreInput.setItems(FXCollections.observableArrayList(Genre.values()));
    		//TODO jak serwer po swojej stronie to obsluzy
//    		userIdColumn.setCellValueFactory(celldata -> celldata.getValue().getId().asString());
//    		userFirstName.setCellValueFactory(celldata -> celldata.getValue().getFirstname());
//    		userLastName.setCellValueFactory(celldata -> celldata.getValue().getLastname());
    		    		
    	});
        
    }

    private void loadAllData() {
        List<ArtifactTo> allArtifacts = restServiceClient.getArtifacts(null);
        List<ArtifactModel> rows = allArtifacts.stream().map(ArtifactModel::fromArtifactTo).collect(Collectors.toList());
        artifacts.setItems(FXCollections.observableArrayList(rows));
        //TODO
        List<UserTo> allUsers = userRestServiceClient.getAllUsers();
  //      List<UserModel> userRows = allUsers.stream().map(UserModel::fromUserTo).collect(Collectors.toList());
  //      users.setItems(FXCollections.observableArrayList(userRows));
        
        
    }

    @FXML
    public void close() {
        getDialog().close();
    }

    @Override
    public String getResourcePath() {

        return "/com/crashcourse/restclient/controller/ArtifactList.fxml";
    }


	@FXML public void reserve() {
		ArtifactModel selectedArtificat = artifacts.getSelectionModel().getSelectedItem();
		Long id = selectedArtificat.getId().getValue();
	
		
	
		loadAllData();
	}

	@FXML 
	public void search() {
		SearchCriteriaTo searchCriteria= new SearchCriteriaTo();
		searchCriteria.setTitle(titleInput.getText());
		searchCriteria.setAuthor(authorInput.getText());
		searchCriteria.setGenre(genreInput.getSelectionModel().getSelectedItem());
		List<ArtifactTo> selectedArtifacts = restServiceClient.getArtifacts(searchCriteria);
		List<ArtifactModel> rows = selectedArtifacts.stream().map(ArtifactModel::fromArtifactTo).collect(Collectors.toList());
        artifacts.setItems(FXCollections.observableArrayList(rows));
		
	}

	@FXML public void clear() {
		titleInput.clear();
		authorInput.clear();
	}

	@FXML public void genre() {}


	@FXML public void rent() {
		ArtifactModel selectedArtificat = artifacts.getSelectionModel().getSelectedItem();
		Long id = selectedArtificat.getId().getValue();
		restServiceClient.rent(id);
	}
				
	@FXML public void free() {
		ArtifactModel selectedArtificat = artifacts.getSelectionModel().getSelectedItem();
		Long id = selectedArtificat.getId().getValue();
		restServiceClient.free(id);
	}
				
	@FXML public void book() {
		ArtifactModel selectedArtificat = artifacts.getSelectionModel().getSelectedItem();
		Long id = selectedArtificat.getId().getValue();
		restServiceClient.book(id);
	}
		

				
}
