package com.crashcourse.restclient.controller.basic;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

import com.crashcourse.restclient.controller.ArtifactListAdminController;
import com.crashcourse.restclient.controller.ArtifactListController;
import com.crashcourse.restclient.controller.ArtifactsBaseController;
import com.crashcourse.restclient.controller.ErrorByLoginDialogController;
import com.crashcourse.restclient.controller.ErrorDialogController;
import com.crashcourse.restclient.controller.WelcomeController;
import com.crashcourse.restclient.view.FXMLDialog;

import javafx.stage.Stage;

@Configuration
@Lazy
public class ScreensConfiguration {

    @Autowired
    private DialogManager manager;

    private Stage primaryStage;

    private Locale locale;

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void showDialog(FXMLDialog fxmlDialog) {
        primaryStage.setScene(fxmlDialog.getScene());
        primaryStage.show();
    }

    @Bean
    @Scope("prototype")
    public FXMLDialog loginDialog() {

        return manager.initializeDialog(welcomeController());
    }

    @Bean
    @Scope("prototype")
    WelcomeController welcomeController() {
        return new WelcomeController(primaryStage);
    }


    @Bean
    @Scope("prototype")
    ArtifactsBaseController artifactListControllerForUser() {
        return new ArtifactListController(primaryStage);
    }
    
    @Bean
    @Scope("prototype")
    ArtifactsBaseController artifactListControllerForAdmin() {
        return new ArtifactListAdminController(primaryStage);
    }
    
    @Bean
    @Scope("prototype")
    ArtifactsBaseController errorController(String message) {
        return new ErrorDialogController(primaryStage, message);
    }
    
    @Bean
    @Scope("prototype")
    ArtifactsBaseController errorByLoginController(String message) {
        return new ErrorByLoginDialogController(primaryStage, message);
    }

    @Bean
    @Scope("prototype")
    public FXMLDialog artifactListDialogForUser() {
        return manager.initializeDialog(artifactListControllerForUser());
    }
    
    @Bean
    @Scope("prototype")
    public FXMLDialog artifactListDialogForAdmin() {
        return manager.initializeDialog(artifactListControllerForAdmin());
    }
    
    @Bean
    @Scope("prototype")
    public FXMLDialog errorDialog(String message) {
        return manager.initializeDialog(errorController(message));
    }
    
    @Bean
    @Scope("prototype")
    public FXMLDialog errorByLoginDialog(String message) {
        return manager.initializeDialog(errorByLoginController(message));
    }
    
    @Bean
    @Scope("prototype")
    public FXMLDialog welcomeDialog() {
        return manager.initializeDialog(welcomeController());
    }


    public void setLocale(Locale locale) {
        this.locale = locale;

    }

    public Locale getLocale() {
        return locale;
    }

}
