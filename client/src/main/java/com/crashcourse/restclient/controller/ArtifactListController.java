package com.crashcourse.restclient.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.naming.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;

import com.crashcourse.restclient.api.ArtifactRestServiceClient;
import com.crashcourse.restclient.datatype.ArtifactTo;
import com.crashcourse.restclient.datatype.SearchCriteriaTo;
import com.crashcourse.restclient.datatype.enumeration.Genre;
import com.crashcourse.restclient.datatype.enumeration.Status;
import com.crashcourse.restclient.exceptions.AlreadyBookedException;
import com.crashcourse.restclient.model.ArtifactModel;
import com.crashcourse.restclient.view.FXMLDialog;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ArtifactListController extends ArtifactsBaseController {

    @FXML
    private TableView<ArtifactModel> artifacts;
    @FXML 
    TableColumn<ArtifactModel, String> idColumn;
    @FXML
    TableColumn<ArtifactModel, String> titleColumn;
    @FXML
    TableColumn<ArtifactModel, String> authorColumn;
    @FXML
    TableColumn<ArtifactModel, Genre> genreColumn;
    @FXML
    TableColumn<ArtifactModel, Status> statusColumn;
    @FXML
    TextField authorInput;
    @FXML
    ComboBox<Genre> genreInput;
    @Autowired
    private ArtifactRestServiceClient restServiceClient;
    @FXML
    TextField titleInput;
	

    public ArtifactListController(Stage primaryStage) {
        super(primaryStage);
    }

    @FXML
    void initialize() {
    	//sprzezenie danych z modeluem
    	
    	//pusczenie metdy na oddzielny watku
    	Platform.runLater(()->{
    		
    		loadAllData();

    		idColumn.setCellValueFactory(celldata -> celldata.getValue().getId().asString());
    		titleColumn.setCellValueFactory(celldata -> celldata.getValue().getTitle());
    		authorColumn.setCellValueFactory(celldata -> celldata.getValue().getAuthor());
    		genreColumn.setCellValueFactory(celldata -> celldata.getValue().getGenre());
    		statusColumn.setCellValueFactory(celldata -> celldata.getValue().getStatus());
    		genreInput.setItems(FXCollections.observableArrayList(Genre.values()));
    		
    		
    	});
        
    }

    private void loadAllData() {
        List<ArtifactTo> allArtifacts = restServiceClient.getArtifacts(null);
        List<ArtifactModel> rows = allArtifacts.stream().map(ArtifactModel::fromArtifactTo).collect(Collectors.toList());
        artifacts.setItems(FXCollections.observableArrayList(rows));
    }

    @FXML
    public void close() {
        getDialog().close();
    }

    @Override
    public String getResourcePath() {

        return "/com/crashcourse/restclient/controller/ArtifactListUser.fxml";
    }


	@FXML 
	public void reserve() {
		ArtifactModel selectedArtificat = artifacts.getSelectionModel().getSelectedItem();
		if(selectedArtificat == null){
			FXMLDialog defaultDialog = getScreens().errorDialog("No item selected");
			getDialog().close();
		    getScreens().showDialog(defaultDialog);
		}else{
			Long id = selectedArtificat.getId().getValue();
			try {
				restServiceClient.reserve(id);
				loadAllData();
			} catch (AuthenticationException e) {
				FXMLDialog defaultDialog = getScreens().errorDialog("Authentification Error");
				getDialog().close();
			    getScreens().showDialog(defaultDialog);
			} catch (AlreadyBookedException e){
				FXMLDialog defaultDialog = getScreens().errorDialog("Already Book");
				getDialog().close();
			    getScreens().showDialog(defaultDialog);
			}
		}
	}

	@FXML 
	public void search() {
		SearchCriteriaTo searchCriteria= new SearchCriteriaTo();
		searchCriteria.setTitle(titleInput.getText());
		searchCriteria.setAuthor(authorInput.getText());
		searchCriteria.setGenre(genreInput.getSelectionModel().getSelectedItem());
		List<ArtifactTo> selectedArtifacts = restServiceClient.getArtifacts(searchCriteria);
		List<ArtifactModel> rows = selectedArtifacts.stream().map(ArtifactModel::fromArtifactTo).collect(Collectors.toList());
        artifacts.setItems(FXCollections.observableArrayList(rows));
		
	}

	@FXML public void clear() {
		titleInput.clear();
		authorInput.clear();
	}

	@FXML public void genre() {}
				

				
}
