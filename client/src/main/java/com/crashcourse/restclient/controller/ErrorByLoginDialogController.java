package com.crashcourse.restclient.controller;



import com.crashcourse.restclient.datatype.enumeration.Genre;
import com.crashcourse.restclient.view.FXMLDialog;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ErrorByLoginDialogController extends ArtifactsBaseController {
	
	@FXML
    Label errorMassage;
	
	private String message;
	
    public ErrorByLoginDialogController(Stage primaryStage, String message) {
        super(primaryStage);
        this.message = message;
    }
    
    @FXML
    void initialize() {
    	errorMassage.setText(message);
    }

    @FXML
    public void close() {
    	FXMLDialog defaultDialog = getScreens().welcomeDialog();
        getDialog().close();
        getScreens().showDialog(defaultDialog);
    }

    @Override
    public String getResourcePath() {
        return "/com/crashcourse/restclient/controller/ErrorDialog.fxml";
    }

}
