package com.crashcourse.restclient.model;

import com.crashcourse.restclient.datatype.ArtifactTo;
import com.crashcourse.restclient.datatype.enumeration.Genre;
import com.crashcourse.restclient.datatype.enumeration.Status;

import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ArtifactModel {

	private LongProperty id;
    private StringProperty title;
    private StringProperty author;
    private ObjectProperty<Genre> genre;   
    private ObjectProperty<Status> status;

    
    public void setId(LongProperty id) {
		this.id = id;
	}

	public void setTitle(StringProperty title) {
		this.title = title;
	}

	public void setAuthor(StringProperty author) {
		this.author = author;
	}

	public void setGenre(ObjectProperty<Genre> genre) {
		this.genre = genre;
	}

	public void setStatus(ObjectProperty<Status> status) {
		this.status = status;
	}

	public LongProperty getId() {
		return id;
	}

	public StringProperty getTitle() {
		return title;
	}

	public StringProperty getAuthor() {
		return author;
	}

	public ObjectProperty<Genre> getGenre() {
		return genre;
	}

	public ObjectProperty<Status> getStatus() {
        return status;
    }


    public static ArtifactModel fromArtifactTo(ArtifactTo artifactTo) {
        ArtifactModel artifactModel = new ArtifactModel();
        artifactModel.id = new SimpleLongProperty(artifactTo.getId());
        artifactModel.title = new SimpleStringProperty(artifactTo.getTitle());
        artifactModel.author = new SimpleStringProperty(artifactTo.getAuthor());
        artifactModel.genre = new SimpleObjectProperty<Genre>(artifactTo.getGenre());
        artifactModel.status = new SimpleObjectProperty<Status>(artifactTo.getStatus());
        return artifactModel;
    }

	
	

	
}
