package com.crashcourse.restclient.model;

import com.crashcourse.restclient.datatype.UserTo;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class UserModel {

	
	private LongProperty id;
	private StringProperty username;
	private StringProperty password;
	private StringProperty email;
	private StringProperty firstname;
	private StringProperty lastname;
	private StringProperty lifeMotto;
	private StringProperty aboutMe;
	
	
	public LongProperty getId() {
		return id;
	}
	public void setId(LongProperty id) {
		this.id = id;
	}
	public StringProperty getUsername() {
		return username;
	}
	public void setUsername(StringProperty username) {
		this.username = username;
	}
	public StringProperty getPassword() {
		return password;
	}
	public void setPassword(StringProperty password) {
		this.password = password;
	}
	public StringProperty getEmail() {
		return email;
	}
	public void setEmail(StringProperty email) {
		this.email = email;
	}
	public StringProperty getFirstname() {
		return firstname;
	}
	public void setFirstname(StringProperty firstname) {
		this.firstname = firstname;
	}
	public StringProperty getLastname() {
		return lastname;
	}
	public void setLastname(StringProperty lastname) {
		this.lastname = lastname;
	}
	public StringProperty getLifeMotto() {
		return lifeMotto;
	}
	public void setLifeMotto(StringProperty lifeMotto) {
		this.lifeMotto = lifeMotto;
	}
	public StringProperty getAboutMe() {
		return aboutMe;
	}
	public void setAboutMe(StringProperty aboutMe) {
		this.aboutMe = aboutMe;
	}

	public static UserModel fromUserTo(UserTo userTo){
		UserModel userModel = new UserModel();
		 userModel.id = new SimpleLongProperty(userTo.getId());
	        userModel.username = new SimpleStringProperty(userTo.getUsername());
	        userModel.password = new SimpleStringProperty(userTo.getPassword());
	        userModel.email = new SimpleStringProperty(userTo.getEmail());
	        userModel.firstname = new SimpleStringProperty(userTo.getFirstname());
	        userModel.lastname = new SimpleStringProperty(userTo.getLastname());
	        userModel.lifeMotto = new SimpleStringProperty(userTo.getLifeMotto());
	        userModel.aboutMe = new SimpleStringProperty(userTo.getAboutMe());
	        return userModel;
	}
	
	

}
