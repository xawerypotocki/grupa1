package com.crashcourse.restclient.datatype;

import com.crashcourse.restclient.datatype.enumeration.Genre;
import com.crashcourse.restclient.datatype.enumeration.Status;

public class SearchCriteriaTo {
	
	private String author;

	private String title;

	private Genre genre;
	
	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

}
