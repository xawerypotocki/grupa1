package com.crashcourse.restclient.datatype.enumeration;

public enum Status {
    AVAILABLE, BOOKED, BORROWED
}
