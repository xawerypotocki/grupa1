package com.crashcourse.restclient.datatype;

import java.util.UUID;

import com.crashcourse.restclient.datatype.enumeration.UserType;

public class SessionTo {
    private UUID sessionId;
    
    private UserType userType;

    public SessionTo() {
    }

    public SessionTo(UUID sessionId) {
        this.sessionId = sessionId;
    }

    public UUID getSessionId() {
        return sessionId;
    }

    public void setSessionId(UUID sessionId) {
        this.sessionId = sessionId;
    }
    
    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userTyp) {
        this.userType = userTyp;
    }
}
