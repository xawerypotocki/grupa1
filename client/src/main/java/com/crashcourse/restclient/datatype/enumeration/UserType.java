package com.crashcourse.restclient.datatype.enumeration;

public enum UserType {
	ADMIN, USER
}
