package com.crashcourse.restclient.exceptions;

public class NoItemSelected extends Exception {
	public NoItemSelected() {
	}

	public NoItemSelected(String message) {
		super(message);
	}
}
